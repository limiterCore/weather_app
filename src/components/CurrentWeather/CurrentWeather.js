import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

// Components
import Loader from '../Loader/Loader';

// Icons
import { ReactComponent as IconLoader } from '../../static/assets/icon-loader.svg';
import { ReactComponent as IconTemp } from '../../static/assets/icon-temperature.svg';
import { ReactComponent as IconPrecip } from '../../static/assets/icon-precip.svg';

// Misc
import { useInterval } from '../../shared/setInterval';

// Styles
import './CurrentWeather.scss';

const CurrentWeather = ({ className }) => {
  const [weather, setWeather] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch('/api/weather/current/')
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        return null;
      })
      .then(res => {
        if (res) {
          setWeather(res);
        } else {
          throw new Error('Failed request');
        }
      })
      .catch(e => {
        setError(e);
      });
  }, []);

  useInterval(() => {
    setIsLoading(true);
    fetch(`${process.env.REACT_APP_EXPRESS_URL}/weather/current/`)
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        return null;
      })
      .then(res => {
        setIsLoading(false);
        if (res) {
          setWeather(res);
        } else {
          throw new Error('Request failed');
        }
      })
      .catch(e => {
        setError(e);
      });
  }, 1000 * 60);

  return (
    <div
      className={cx('current-weather', {
        [className]: className,
      })}
    >
      {error && <div className="current-weather__error">{error.message}</div>}

      {!weather && !error && <Loader className="current-weather__loading-container" />}

      {weather && !error && (
        <div className="current-weather__values">
          <div className="current-weather__temp">
            <IconTemp className="current-weather__temp-icon" />
            <div className="current-weather__temp-values">
              <span className="current-weather__temp-value" data-cy="current-weather-temp">
                {weather.temperature}
              </span>
              <span className="current-weather__temp-unit">&deg;</span>
            </div>
          </div>

          <div className="current-weather__precip">
            <IconPrecip className="current-weather__precip-icon" />
            <div className="current-weather__precip-values">
              <span className="current-weather__precip-value" data-cy="current-weather-precip">
                {weather.precipitation}
              </span>
              <span className="current-weather__precip-unit">mm/hour</span>
            </div>
          </div>
        </div>
      )}

      {isLoading && <IconLoader className="current-weather__mini-loader" />}
    </div>
  );
};

CurrentWeather.propTypes = {
  className: PropTypes.string,
};

CurrentWeather.defaultProps = {
  className: '',
};

export default CurrentWeather;
