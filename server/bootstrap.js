require('dotenv').config();
const request = require('request-promise-native');
const mongoose = require('mongoose');

/** Connecting to mongo */
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
require('./models/DayEntry');
require('./models/HourEntry');
require('./models/Current');

const DayEntry = mongoose.model('DayEntry');
const HourEntry = mongoose.model('HourEntry');
const Current = mongoose.model('Current');

/* eslint-disable no-console */

const bootstrapDatabase = async () => {
  const key = process.env.WEATHER_APP_KEY;
  const placeCoordinates = process.env.WEATHER_APP_COORDINATES;

  /** Clear days and hours entries */
  try {
    await Promise.all([
      DayEntry.deleteMany({}).exec(),
      HourEntry.deleteMany({}).exec(),
      Current.deleteMany({}).exec(),
    ]);
  } catch (error) {
    console.log(error);
  }

  const now = new Date();
  now.setHours(now.getHours() - 1);

  const today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);

  /** Generate 7 days */
  const times = new Array(7).fill(true).map((item, index) => {
    const date = new Date(today.getTime());
    date.setDate(date.getDate() - index);
    return new Date(date.getTime());
  });

  /** Populate days and hours for week */
  const fill = () =>
    new Promise(resolve => {
      times.forEach(async (time, index) => {
        const timeString = `${time.toISOString().split('.')[0]}Z`;
        const options = {
          method: 'GET',
          uri: `https://api.darksky.net/forecast/${key}/${placeCoordinates},${timeString}?units=si`,
        };

        /** Make a request to WEATHER API */
        let response;
        try {
          response = await request(options);
          response = JSON.parse(response);
        } catch (error) {
          console.log(error);
        }

        if (!response.daily || !response.daily.data || !response.daily.data.length) {
          console.log(new Error('No daily data'));
        }

        if (!response.hourly || !response.hourly.data || !response.hourly.data.length) {
          console.log(new Error('No hourly data'));
        }

        /** Create a day entry */
        const daily = response.daily.data[0];

        let dayEntry;

        /** Add only past days */
        if (time < today) {
          dayEntry = new DayEntry({
            date: time.toISOString(),
            temperature: (daily.temperatureHigh + daily.temperatureLow) / 2,
            precipitation: daily.precipIntensity || 0,
          });
        } else {
          dayEntry = null;
        }

        /** Create hour entries */
        const hourEntries = [];
        response.hourly.data.forEach(entry => {
          /** Add only past hours */
          if (new Date(entry.time * 1000) < now) {
            hourEntries.push(
              new HourEntry({
                date: new Date(entry.time * 1000),
                temperature: entry.temperature,
                precipitation: entry.precipIntensity || 0,
              }),
            );
          }
        });

        /** Save all the entries to the database */
        try {
          await Promise.all([
            dayEntry ? dayEntry.save() : Promise.resolve(null),
            ...hourEntries.map(entry => entry.save()),
          ]);
        } catch (error) {
          console.log(error);
        }

        if (index === times.length - 1) {
          resolve();
        }
      });
    });

  await fill();

  /** Fill current weather */
  const options = {
    method: 'GET',
    uri: `https://api.darksky.net/forecast/${key}/${placeCoordinates}?units=si`,
  };

  try {
    let result = await request(options);
    result = JSON.parse(result);

    if (!result || !result.currently) {
      console.log(new Error('No currently data'));
    }

    const current = new Current({
      date: new Date(),
      temperature: result.currently.temperature,
      precipitation: result.currently.precipIntensity || 0,
    });

    await current.save();
  } catch (error) {
    console.log(error);
  }

  console.log('Database has been bootstrapped');
};

(async () => {
  await bootstrapDatabase();
})();
