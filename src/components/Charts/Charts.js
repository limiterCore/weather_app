import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import { Chart } from 'react-google-charts';

import Loader from '../Loader/Loader';

import { weekDays } from '../../shared/weekDays';

import './Charts.scss';
import { useInterval } from '../../shared/setInterval';

const hourlyOptions = {
  title: 'Last 24 hours',
  vAxes: {
    0: { logScale: false, title: 'Precip., mm/h.', minValue: 0 },
    1: { logScale: false, title: 'Temp., celsius' },
  },
  hAxis: { title: 'Hour' },
  series: {
    0: { type: 'bars', targetAxisIndex: 0, color: '#1c5671' },
    1: { type: 'line', targetAxisIndex: 1, color: '#ff5358' },
  },
};

const dailyOptions = {
  ...hourlyOptions,
  title: 'Last week',
  hAxis: { title: 'Day' },
};

const Charts = () => {
  const [hourlyData, setHourlyData] = useState(null);
  const [dailyData, setDailyData] = useState(null);
  const [currentTab, setCurrentTab] = useState('hourly');
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const loadData = () => {
    fetch(`${process.env.REACT_APP_EXPRESS_URL}/weather/data/`)
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        return null;
      })
      .then(res => {
        setIsLoading(false);

        if (!res || !res.hours || !res.days) {
          throw new Error('Failed request');
        }

        const parsedHours = res.hours.map(item => {
          const date = new Date(Date.parse(item.date));
          const hour = date
            .getHours()
            .toString()
            .padStart(2, '0');

          return [hour, item.precipitation, item.temperature];
        });

        const parsedDays = res.days.map(item => {
          const date = new Date(Date.parse(item.date));

          return [
            `${weekDays[date.getUTCDay()]}/${date
              .getDate()
              .toString()
              .padStart(2, '0')}`,
            item.precipitation,
            item.temperature,
          ];
        });

        setHourlyData([['Hour', 'Precip.', 'Temperature'], ...parsedHours]);

        setDailyData([['Day', 'Precip.', 'Temperature'], ...parsedDays]);
      })
      .catch(e => {
        setIsLoading(false);
        setError(e);
      });
  };

  /** Load data initially after component is loaded */
  useEffect(loadData, []);

  /** Update data on every n minutes */
  useInterval(loadData, 1000 * 60 * 2);

  return (
    <div className="charts">
      {error && <div className="charts__error">{error.message}</div>}

      {isLoading && <Loader className="charts__loading-container" inverted />}

      {!isLoading && !error && hourlyData && dailyData && (
        <div className="charts__container">
          <div className="charts__chart-toggle">
            <button
              type="button"
              className="charts__chart-toggle-button"
              onClick={() => {
                setCurrentTab('daily');
              }}
              disabled={currentTab === 'daily'}
              data-cy="charts-toggle-button-daily"
            >
              Last week
            </button>
            <button
              type="button"
              className="charts__chart-toggle-button"
              onClick={() => {
                setCurrentTab('hourly');
              }}
              disabled={currentTab === 'hourly'}
              data-cy="charts-toggle-button-hourly"
            >
              Last 24 hours
            </button>
          </div>
          <div className="charts__charts">
            <div className="charts__charts-item charts__charts-item_visible">
              <Chart
                chartType="ComboChart"
                data={hourlyData}
                options={hourlyOptions}
                width="100%"
                height="400px"
                legendToggle
              />
            </div>
            <div
              className={cx('charts__charts-item charts__charts-item_absolute', {
                'charts__charts-item_visible': currentTab === 'daily',
              })}
            >
              <Chart
                chartType="ComboChart"
                data={dailyData}
                options={dailyOptions}
                width="100%"
                height="400px"
                legendToggle
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Charts;
