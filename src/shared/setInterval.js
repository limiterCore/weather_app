import React, { useEffect } from 'react';

export const useInterval = (callback, ms) => {
  const savedCallback = React.useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    let id;
    if (ms !== null) {
      id = setInterval(tick, ms);
      return () => clearInterval(id);
    }
  }, [ms]);
};
