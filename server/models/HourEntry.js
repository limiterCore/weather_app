const mongoose = require('mongoose');

const HourEntrySchema = new mongoose.Schema({
  date: Date,
  hour: Number,
  temperature: Number,
  precipitation: Number,
});

mongoose.model('HourEntry', HourEntrySchema);
