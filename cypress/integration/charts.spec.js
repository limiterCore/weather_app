describe('Screen test', function() {
  let polyfill;

  before(() => {
    const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js';
    cy.request(polyfillUrl).then(response => {
      polyfill = response.body;
    });
  });

  it('We should see the charts and have ability to change a view', () => {
    cy.server();
    cy.route('GET', '/api/weather/data/').as('data');
    cy.route('GET', '/api/weather/current/').as('current');

    cy.visit('/', {
      onBeforeLoad(win) {
        /* eslint-disable no-param-reassign */
        delete win.fetch;
        win.eval(polyfill);
        win.fetch = win.unfetch;
        /* eslint-enable */
      },
    });

    /** Wait for response from `current` */
    cy.wait('@current').then(res => {
      /** Check that the response renders correctly */
      cy.get('[data-cy=current-weather-temp]').should(
        'have.text',
        `${res.response.body.temperature}`,
      );
      cy.get('[data-cy=current-weather-precip]').should(
        'have.text',
        `${res.response.body.precipitation}`,
      );
    });

    /** Wait for response from `data` */
    cy.wait('@data');

    /** Wait for chart to draw */
    cy.wait(2000);

    /** Check last week button */
    cy.get('[data-cy=charts-toggle-button-daily]')
      .should('have.text', 'Last week')
      .and('to.not.have.property', 'disabled');

    /** Check last 24 hours button */
    cy.get('[data-cy=charts-toggle-button-hourly]')
      .should('have.text', 'Last 24 hours')
      .and('to.have.prop', 'disabled');

    /** Check currently visible chart */
    cy.get('text')
      .contains('Hour')
      .should('exist');

    /** Toggle view */
    cy.get('[data-cy=charts-toggle-button-daily]').click();

    /** Check new disabled properties */
    cy.get('[data-cy=charts-toggle-button-daily]').should('have.prop', 'disabled');
    cy.get('[data-cy=charts-toggle-button-hourly').should('to.not.have.property', 'disabled');

    /** Check currently visible chart */
    cy.get('text')
      .contains('Day')
      .should('exist');

    /** And toggle the view back */
    cy.get('[data-cy=charts-toggle-button-hourly]').click();

    /** Check new disabled properties */
    cy.get('[data-cy=charts-toggle-button-hourly]').should('have.prop', 'disabled');
    cy.get('[data-cy=charts-toggle-button-daily').should('to.not.have.property', 'disabled');

    /** Check currently visible chart */
    cy.get('text')
      .contains('Hour')
      .should('exist');
  });
});
