const mongoose = require('mongoose');

const DayEntrySchema = new mongoose.Schema({
  date: Date,
  temperature: Number,
  precipitation: Number,
});

mongoose.model('DayEntry', DayEntrySchema);
