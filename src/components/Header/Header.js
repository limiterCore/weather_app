import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import CurrentWeather from '../CurrentWeather/CurrentWeather';
import { useInterval } from '../../shared/setInterval';
import './Header.scss';

const Header = ({ className }) => {
  const [date, setDate] = useState(Date.now());

  useInterval(() => {
    setDate(Date.now());
  }, 1000);

  const currentDate = new Date(date);
  const day = `${currentDate.getDate()}`.padStart(2, '0');
  const month = `${currentDate.getMonth() + 1}`.padStart(2, '0');
  const year = currentDate.getFullYear();
  const hours = `${currentDate.getHours()}`.padStart(2, '0');
  const minutes = `${currentDate.getMinutes()}`.padStart(2, '0');
  const seconds = `${currentDate.getSeconds()}`.padStart(2, '0');

  return (
    <header
      className={cx('header', {
        [className]: className,
      })}
    >
      <div className="header__container">
        <CurrentWeather className="header__current-weather" />
        <div className="header__date-time">
          {`${day}.${month}.${year}`}
          &nbsp;
          {`${hours}:${minutes}.${seconds}`}
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  className: PropTypes.string,
};

Header.defaultProps = {
  className: '',
};

export default Header;
