import React from 'react';

import Header from '../Header/Header';
import Charts from '../Charts/Charts';

import './App.scss';

const App = () => {
  return (
    <div className="app">
      <Header className="app__header" />
      <Charts />
    </div>
  );
};

export default App;
