require('dotenv').config();
const request = require('request-promise-native');
const mongoose = require('mongoose');

/** Connecting to mongo */
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
require('./models/DayEntry');
require('./models/HourEntry');
require('./models/Current');

const DayEntry = mongoose.model('DayEntry');
const HourEntry = mongoose.model('HourEntry');
const Current = mongoose.model('Current');

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

/** Handler for populating database */
const task = async () => {
  const key = process.env.WEATHER_APP_KEY;
  const placeCoordinates = process.env.WEATHER_APP_COORDINATES;

  const nowMinus1Hour = new Date();
  nowMinus1Hour.setHours(nowMinus1Hour.getHours() - 1);

  const today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);

  /** Make a request to WEATHER API */
  const options = {
    method: 'GET',
    uri: `https://api.darksky.net/forecast/${key}/${placeCoordinates}?units=si`,
  };

  let response;
  try {
    response = await request(options);
    response = JSON.parse(response);
  } catch (error) {
    console.log(error.message);
    return;
  }

  /** Fill current weather */
  if (response && response.currently) {
    const prevVersion = await Current.findOne();
    prevVersion.temperature = response.currently.temperature;
    prevVersion.precipitation = response.currently.precipIntensity || 0;

    await prevVersion.save();
    console.log('Updated current weather');
  }

  /** Make a request to the WEATHER TIME MACHINE API */
  const timeString = `${new Date().toISOString().split('.')[0]}Z`;
  const optionsToday = {
    method: 'GET',
    uri: `https://api.darksky.net/forecast/${key}/${placeCoordinates},${timeString}?units=si`,
  };

  let responseToday;
  try {
    responseToday = await request(optionsToday);
    responseToday = JSON.parse(responseToday);
  } catch (error) {
    return next(error);
  }

  /** Create a day entry */
  let dayEntry = null;
  if (responseToday && responseToday.daily) {
    const daily = responseToday.daily.data[0];

    const date = new Date(daily.time * 1000);
    const dateStart = new Date(date.getTime());
    dateStart.setHours(0);
    dateStart.setMinutes(0);
    dateStart.setSeconds(0);

    const dateNext = new Date(dateStart.getTime());
    dateNext.setDate(dateNext.getDate() + 1);

    const foundDate = await DayEntry.findOne({
      date: {
        $gte: dateStart,
        $lt: dateNext,
      },
    });

    /** Update daily data */
    if (foundDate) {
      foundDate.temperature = (daily.temperatureHigh + daily.temperatureLow) / 2;
      foundDate.precipitation = daily.precipIntensity || 0;
      await foundDate.save();
      console.log(`Updated entry for date ${date.toISOString()}`);
    } else {
      /** Insert new daily date */
      dayEntry = new DayEntry({
        date,
        temperature: (daily.temperatureHigh + daily.temperatureLow) / 2,
        precipitation: daily.precipIntensity || 0,
      });
      console.log(`Created entry for date ${date.toISOString()}`);
    }
  }

  /** Create hour entries for new hours */
  const hourEntries = [];
  if (responseToday && responseToday.hourly) {
    const iterateHours = async () => {
      await asyncForEach(responseToday.hourly.data, async entry => {
        const date = new Date(entry.time * 1000);
        const foundHour = await HourEntry.findOne({
          date,
        });

        /** Add only past hours that haven't been already added */
        if (!foundHour && date < nowMinus1Hour) {
          hourEntries.push(
            new HourEntry({
              date: new Date(entry.time * 1000),
              temperature: entry.temperature,
              precipitation: entry.precipIntensity || 0,
            }),
          );
          console.log(`Created entry for hour ${new Date(entry.time * 1000)}`);
        }
      });
    };

    await iterateHours();
  }

  /** Save all the entries to the database */
  try {
    await Promise.all([
      dayEntry ? dayEntry.save() : Promise.resolve(null),
      ...hourEntries.map(entry => entry.save()),
    ]);
  } catch (error) {
    console.log(error.message);
  }
};

module.exports = task;
