const mongoose = require('mongoose');

const CurrentSchema = new mongoose.Schema({
  date: Date,
  temperature: Number,
  precipitation: Number,
});

mongoose.model('Current', CurrentSchema);
