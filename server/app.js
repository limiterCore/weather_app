require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const errorhandler = require('errorhandler');
const mongoose = require('mongoose');

const scheduler = require('./scheduler');

const app = express();
const port = 1429;

/** Normal express config defaults */
app.use(require('morgan')('dev'));

app.use(bodyParser.json());

if (process.env.NODE_ENV !== 'production') {
  app.use(errorhandler());
}

/** Connecting to mongo */
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
require('./models/DayEntry');
require('./models/HourEntry');
require('./models/Current');

const DayEntry = mongoose.model('DayEntry');
const HourEntry = mongoose.model('HourEntry');
const Current = mongoose.model('Current');

/** Run scheduler */
setInterval(scheduler, 1000 * 60 * 5);

/** Handler for getting the current weather */
app.get('/api/weather/current/', async (req, res) => {
  const currentWeather = await Current.findOne();

  if (!currentWeather) {
    return res.status(404).json({
      errors: {
        message: 'Current weather not found',
        error: {
          code: 404,
        },
      },
    });
  }

  return res.json({
    temperature: currentWeather.temperature,
    precipitation: currentWeather.precipitation,
  });
});

/** Handler for getting weather for the last 24 hours and week */
app.get('/api/weather/data/', (req, res) => {
  const now = new Date();
  now.setHours(now.getHours() - 1);
  const last24Hours = HourEntry.find({
    date: {
      $lt: now,
    },
  })
    .sort({ date: -1, hour: -1 })
    .limit(24)
    .exec();

  const today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);

  const lastWeek = DayEntry.find({
    date: {
      $lt: today,
    },
  })
    .sort({ date: -1 })
    .limit(6)
    .exec();

  Promise.all([last24Hours, lastWeek]).then(result => {
    res.json({
      hours: result[0].reverse(),
      days: result[1].reverse(),
    });
  });
});

/** catch 404 and forward to error handler */
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/** development error handler will print stacktrace */
if (process.env.NODE_ENV !== 'production') {
  app.use((err, req, res, next) => {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

/** production error handler no stack traces leaked to user */
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: err,
    },
  });
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
